var app = new function(){
    var base_url = 'http://localhost:82/ShapesApp/';
    var Shapes = [
                    'Circle',
                    'Square',
                    'Rectangle'
                ];
    
    this.init = function(container){
        var list = $('<ul></ul>').attr({
                        'id' : 'shapesList'
                    });
        $.each(Shapes, function(i, shape){
            var li_ele = $('<li></li>').append($('<a></a>').attr({
                                'data-id' : shape,
                                'href' : '#'
                            }).text(shape));
            list.append(li_ele);
        });
        $(container).append(list);
        RegisterEvents();
    };

    var RegisterEvents = function(){
        clickShapeNameEvent();
    };

    var clickShapeNameEvent = function(){
        $('ul#shapesList li a').on('click',function(e){
            e.preventDefault();
            var shape = $(this).attr('data-id');
            getShapeParams(shape);
        });
    };

    var getShapeParams = function(shape){
        $.ajax({
            url: base_url + "api/public/getParams/" + shape,
            dataType: "JSON"
        })
        .done(function( result ) {
            $('#paramCollector').remove();
            if(result.error_status == 0){
                renderParamsCollectionForm(result.data, shape);
            }
            else{
                alert(result.message);
            }
        });
    };

    var renderParamsCollectionForm = function(params, shape){
        var form = $('<form></form>').attr({
            'id': 'paramCollector'
        });      

        $.each(params, function(pname,pdesc){
            var label = $('<label></label>').text(pname);
            var fld_grp = $('<div></div>').attr({
                'class':'fld_grp'
            });
            var fld = $('<input></input>').attr({
                'type':'number',
                'name':pname
            });
            var desc = $('<span></span>').attr({
                'class': 'pdesc'
            }).text(pdesc);

            fld_grp.append(label).append(fld).append(desc);
            form.append(fld_grp);
        });
        form.append($('<div></div>').attr({
                            'class':'fld_grp'
                        }).append($('<input></input>').attr({
                            'type': 'submit',
                            'value': 'calculate'
                        })));
        $('ul#shapesList').after(form);
        shapeParamsSubmitEvent(shape);
    }

    var shapeParamsSubmitEvent = function(shape){
        $('form#paramCollector').on('submit',function(e){
            e.preventDefault();
            var form_data = $( this ).serialize();
            $.ajax({
                url: base_url + "api/public/" + shape + '/calculate',
                data: form_data,
                dataType: "JSON"
            })
            .done(function( result ) {
                if(result.error_status == 0){
                    alert('Area = '+result.data.area+' Perimeter = '+result.data.perimeter);
                }
                else{
                    alert(result.message);
                }
            });
        });
    }
};