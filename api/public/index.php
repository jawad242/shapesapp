<?php
    // Require Autoloader
    require_once '../../vendor/autoload.php';

    use jhumayun\Shapes\core\ShapesFactory;

    $app = new Slim\App();

    $app->get('/getParams/{shape}', function ($request, $response, $args) {
        $output = array(
            'error_status' => 0,
            'message' => '',
            'data' => array()
        );
        $http_status = 200;
        try{
            $factory = new ShapesFactory();
            $shape = $factory::create($args['shape']);
            $output['data'] = $shape->getParams();
        }
        catch(\Exception $e){
            $output['error_status'] = 1;
            $output['message'] = 'Caught Exception: '.$e->getMessage();
        }
        return $response->withJson($output,$http_status);
    });

    $app->get('/{shape}/calculate', function ($request, $response, $args) {
        $output = array(
            'error_status' => 0,
            'message' => '',
            'data' => array()
        );
        $http_status = 200;
        try{
            $factory = new ShapesFactory();
            $shape = $factory::create($args['shape'],$request->getQueryParams());
            $output['data'] = array(
                'area' => $shape->calculateArea(),
                'perimeter' => $shape->calculatePerimeter()
            );
        }
        catch(\Exception $e){
            $output['error_status'] = 1;
            $output['message'] = 'Caught Exception: '.$e->getMessage();
            $http_status = 500;
        }
        return $response->withJson($output,$http_status);
    });

    $app->run();
?>